var finalOrder = {};
var orderLineItems = {};
var finalAmount = 0;
var currentItem = {};

$(document).ready(function () {
	loadProducts();

	$("#place-order-button").click(function () {
		fetch("OrderProcessingServlet", {
			method: "POST",
			body: JSON.stringify({ order: finalOrder }),
			headers: { "content-type": "application/json" },
		}).then(function (response) {
			if (response.ok) {
				console.log("Post Non Apple Payment successful !");
			} else {
				console.log("Post Non Apple Payment Post failed !!!");
			}
		});
	});
});

function disableNonApplePayButton(disable) {
	$("#place-order-button").prop("disabled", disable);
}

function displaySelectedItemsDiv(display) {
	if (display) {
		$("#selected-products-div").show();
	} else {
		$("#selected-products-div").hide();
	}
}

function loadProducts() {
	$.getJSON("content/products.json", function (data) {
		var listItems = [];

		$.each(data, function (key, val) {
			var orderLineItem = {
				product: val,
				count: 0,
			};

			orderLineItems[orderLineItem.product.id] = orderLineItem;

			const image = orderLineItem.product.image;

			const priceOptions =
				typeof orderLineItem.product.options == "undefined"
					? `<p>$ ${orderLineItem.product.price / 100} ea.</p>`
					: buildOptions(orderLineItem.product);

			var listItem = `<li class="baseLI">
					<span href="#" style="flex: 1">
						<div class="leftSide">
							<div>
								<img src="content/assets/productImages/${image}"/>
							</div>

							<div>
								<h2>${orderLineItem.product.name}</h2>
								<p>${orderLineItem.product.description}</p>

								${priceOptions}
							</div>
						</div>
					</span>

					<a id="btn_${orderLineItem.product.id}_add" onclick="productAdded(this)"
					 href="#purchase" data-rel="popup" data-position-to="window" data-transition="pop">
						<span href="#" class="rightSide ui-btn ui-corner-all ui-icon-plus ui-btn-icon-notext ui-btn-inline">Plus</span>
					</a>
				</li>`;

			listItems.push(listItem);
		});

		$("#all-products").append(listItems.join(""));
		// Task 2: Add the missing line. Hint: The list may need to be refreshed to reapply the styles as the list is build dynamically instead of static

		// Check if exists items in your basket (memory)
		const uiDA = JSON.parse(localStorage.getItem("ui-da"));

		if (uiDA) {
			orderLineItems = uiDA["orderLineItems"];
			calculatePrice();
			repaintSelectedList();
		} else {
			displaySelectedItemsDiv(false);
			disableNonApplePayButton(true);
		}
	});
}

function buildOptions(product) {
	var optionsHTML = "<p>";

	$.each(product.options, function (size, price) {
		optionsHTML = `${optionsHTML} <button id="btn_size_${product.id}_${size}" onclick="selectSize('${product.id}', '${size}', '${price}')">${size}</button>&nbsp;&nbsp;`;
	});

	optionsHTML = optionsHTML + "</p>";

	return optionsHTML;
}

function resetStyle(id) {
	const styleDefault = {
		backgroundColor: "#E5E5E5",
		borderRadius: "5px",
	};

	if (Object.keys(currentItem)[0] == orderLineItems[id].product.id) {
		document.querySelector("#" + `btn_size_${id}_S`).style = styleDefault;
		document.querySelector("#" + `btn_size_${id}_M`).style = styleDefault;
		document.querySelector("#" + `btn_size_${id}_L`).style = styleDefault;
	}
}

function selectSize(id, size, price) {
	currentItem = { [id]: { size, price } };

	resetStyle(id);

	const idItem = `btn_size_${id}_${size}`;
	const item = document.querySelector("#" + idItem);
	item.style.backgroundColor = "#FF0";

	var orderLineItem = orderLineItems[id];
	orderLineItem.product.price = parseFloat(price);
}

function productAdded(component) {
	var productId = getProductId(component.id);
	var orderLineItem = orderLineItems[productId];

	if (orderLineItem.product.options && !currentItem[orderLineItem.product.id]) {
		alert("First, you need to select a size");
		return;
	}

	orderLineItem.count = orderLineItem.count + 1;
	orderLineItems[productId] = orderLineItem;
	calculatePrice();
	disableNonApplePayButton(false);
	repaintSelectedList();

	resetStyle(productId);

	if (Object.keys(currentItem)[0] == orderLineItems[productId].product.id) {
		currentItem = {};
	}
}

function productRemoved(component) {
	var productId = getProductId(component.id);
	var orderLineItem = orderLineItems[productId];

	if (orderLineItem.count > 0) {
		orderLineItem.count = orderLineItem.count - 1;
		orderLineItems[productId] = orderLineItem;
	}

	calculatePrice();
	repaintSelectedList();
}

function repaintSelectedList() {
	var listSelectedItems = [];

	$.each(orderLineItems, function (key, orderLineItem) {
		if (orderLineItem.count != 0) {
			var listSelectedItem =
				"<li>" +
				'<a href="#">' +
				'<img src="content/assets/productImages/' +
				orderLineItem.product.image +
				'"/>' +
				"<h2>" +
				orderLineItem.product.name +
				"</h2>" +
				"<p>" +
				orderLineItem.count +
				"</p>" +
				'<a id="btn_' +
				orderLineItem.product.id +
				'_add" onclick="productRemoved(this)" href="#purchase" data-rel="popup" data-position-to="window" data-transition="pop">Remove</a>' +
				"</li>";

			listSelectedItems.push(listSelectedItem);
		}
	});

	$("#selected-products").empty();
	$("#selected-products").append(listSelectedItems.join(""));
	$("#selected-products").listview("refresh");

	if (listSelectedItems.length == 0) {
		displaySelectedItemsDiv(false);
		disableNonApplePayButton(true);
	} else {
		displaySelectedItemsDiv(true);
		disableNonApplePayButton(false);
	}
}

function getProductId(componentId) {
	var firstIndex = componentId.indexOf("_") + 1;
	var lastIndex = componentId.lastIndexOf("_");

	return componentId.substring(firstIndex, lastIndex);
}

function calculatePrice() {
	var subTotal = 0.0;
	var finalOrderItems = [];

	$.each(orderLineItems, function (key, orderLineItem) {
		if (orderLineItem.count != 0) {
			subTotal = subTotal + orderLineItem.count * orderLineItem.product.price;
			finalOrderItems.push(orderLineItem);
		}
	});
	var formattedSubTotal = subTotal / 100.0;

	$("#payment_amount").text("$" + formattedSubTotal);

	finalOrder = {
		finalOrderItems: finalOrderItems,
		subTotal: subTotal,
		formattedSubTotal: formattedSubTotal,
	};

	finalAmount = subTotal;
	saveLocalStorage(finalOrder, orderLineItems);
}

function saveLocalStorage(finalOrder, orderLineItems) {
	localStorage.setItem("ui-da", JSON.stringify({ finalOrder, orderLineItems }));
}
